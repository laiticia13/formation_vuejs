
/**
 * retourne la valeur la plus grande des deux paramètres
 * 
 * Contraintes:
 *    - utiliser l'opérateur ternaire (if interdit)
 */
function ternaire(a , b) {
    let valeur = (a > b) ? a : b ;
    
   /* if(a > b)
    {
        return a;
    }
    else{
        return b;
    }*/
        
    return valeur ;
}

console.log(ternaire(800,800));

module.exports = ternaire;