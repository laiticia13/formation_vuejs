
/**
 * Utiliser les ternaires en les chainants, pour éviter les if elseif else
 * 
 * Renvoyer ">" si a supérieur à b
 * Renvoyer "<" si a inférieur à b
 * Renvoyer "=" si a égal à b
 * 
 * Contraintes:
 *    - utiliser l'opérateur ternaire (if interdit)
 */

function ternaryChain(a, b) {
    let valeur = (a > b) ? '>' :
                 (b > a) ? '<' : '=';

    return valeur
}

console.log(ternaryChain(3,3))
;
module.exports = ternaryChain;